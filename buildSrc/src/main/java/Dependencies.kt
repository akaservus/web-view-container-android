object Apps {
    const val compileSdk = 29
    const val minSdk = 26
    const val targetSdk = 29

    const val buildTools = "29.0.2"

    const val versionCode = 1
    const val versionName = "1.0"
}

object Versions {
    const val gradle = "4.0.0"
    const val kotlin = "1.3.72"
    const val appcompat = "1.1.0"
}

object Plugins {
    const val androidApplication = "com.android.application"
    const val androidLibrary = "com.android.library"
    const val android = "android"
    const val androidExtensions = "android.extensions"
    const val jvm = "jvm"
    const val kapt = "kapt"
}

object Libraries {

    object Kotlin {
        const val version = "1.3.72"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"
        const val reflect = "org.jetbrains.kotlin:kotlin-reflect:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
        const val allOpen = "org.jetbrains.kotlin:kotlin-allopen:$version"
    }

    object AndroidX {
        private const val constraintLayoutVersion = "1.1.3"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"

        private const val espressoVersion = "3.2.0"
        const val espresso = "androidx.test.espresso:espresso-core:$espressoVersion"

        private const val ktxVersion = "1.2.0"
        const val ktx = "androidx.core:core-ktx:$ktxVersion"

        private const val testJunitVersion = "1.1.1"
        const val testJunit = "androidx.test.ext:junit:$testJunitVersion"

        private const val webkitVersion = "1.2.0"
        const val webkit = "androidx.webkit:webkit:$webkitVersion"
    }

    const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.gradle}"

    // Coroutines
    private const val coroutinesVersion = "1.3.5"
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion"

    private const val daggerVersion = "2.24"
    const val daggerSupport = "com.google.dagger:dagger-android-support:$daggerVersion"
    const val daggerAndroid = "com.google.dagger:dagger-android:$daggerVersion"
    const val daggerProcessor = "com.google.dagger:dagger-android-processor:$daggerVersion"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:$daggerVersion"

    private const val fileloggerVersion = "1.0.7"
    const val filelogger = "com.github.bosphere.android-filelogger:filelogger:$fileloggerVersion"

    private const val junitVersion = "4.13"
    const val junit = "junit:junit:$junitVersion"

    private const val timberVersion = "4.7.1"
    const val timber = "com.jakewharton.timber:timber:$timberVersion"

    private const val jacksonVersion = "2.11.0"
    const val jackson = "com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion"
}

object TestLibraries {

}
