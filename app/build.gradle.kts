plugins {
    id(Plugins.androidApplication)
    kotlin(Plugins.android)
    kotlin(Plugins.androidExtensions)
    kotlin(Plugins.kapt)
}

android {
    compileSdkVersion(Apps.compileSdk)
    buildToolsVersion(Apps.buildTools)

    defaultConfig {
        // applicationId "de.sowok.webviewcontainer"
        minSdkVersion(Apps.minSdk)
        targetSdkVersion(Apps.targetSdk)
        versionCode = Apps.versionCode
        versionName = Apps.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libraries.Kotlin.stdlib)
    implementation(Libraries.appcompat)
    implementation(Libraries.AndroidX.ktx)
    implementation(Libraries.AndroidX.constraintLayout)

    implementation(project(":webviewcontainer"))

    testImplementation(Libraries.junit)
    androidTestImplementation(Libraries.AndroidX.testJunit)
    androidTestImplementation(Libraries.AndroidX.espresso)
}
