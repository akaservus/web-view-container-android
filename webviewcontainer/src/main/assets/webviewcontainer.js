WebViewContainer = {
    // Hier API Core
    callbacks: {},
    postMessage: function(msg) {
        WebViewContainerHandler.postMessage(msg);
    },
    uuid: function() {
        return 'pxxxxxxxxxxxxxxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    call: function(method, message, callback) {
        var id = this.uuid();
        var msg = { method: method, message: message, id: id };
        this.callbacks[id] = callback;
        this.postMessage(JSON.stringify(msg));
    },
    callback: function(msg) {
        var data = JSON.parse(msg);
        this.callbacks[data.id](data.message);
        delete this.callbacks[data.id];
    },
    // Ab hier die API-Methoden
    test: function(msg, callback) {
        this.call("ping", msg, callback);
    },
    getUIMode: function(callback) {
        this.call("getUIMode", null, callback);
    }
};
console.log('web view container loaded');