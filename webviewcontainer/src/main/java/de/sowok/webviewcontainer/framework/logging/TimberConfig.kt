package de.sowok.webviewcontainer.framework.logging

import de.sowok.webviewcontainer.BuildConfig
import javax.inject.Inject
import javax.inject.Provider
import timber.log.Timber

class TimberConfig @Inject constructor(
    private val releaseTimberTree: Provider<ReleaseTimberTree>,
    private val fileLoggingTimberTree: Provider<FileLoggingTimberTree>
) {
    fun configure() {
        if (BuildConfig.DEBUG) {
            Timber.plant(fileLoggingTimberTree.get())
        } else {
            Timber.plant(releaseTimberTree.get())
        }
    }
}
