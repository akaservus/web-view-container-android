package de.sowok.webviewcontainer.framework.inject

import android.content.Context
import de.sowok.webviewcontainer.framework.inject.module.WebViewContainerModule
import javax.inject.Singleton

@Singleton
class DependencyInjector {
    fun buildComponent(context: Context): WebViewContainerComponent =
        DaggerWebViewContainerComponent.builder()
            .webViewContainerModule(WebViewContainerModule(context))
            .build()
}