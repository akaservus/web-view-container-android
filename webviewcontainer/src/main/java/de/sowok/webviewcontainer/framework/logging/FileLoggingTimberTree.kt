package de.sowok.webviewcontainer.framework.logging

import android.content.Context
import android.util.Log
import com.bosphere.filelogger.FL
import com.bosphere.filelogger.FLConfig
import com.bosphere.filelogger.FLConst
import de.sowok.webviewcontainer.framework.annotation.LogPath
import java.io.File
import javax.inject.Inject
import timber.log.Timber

open class FileLoggingTimberTree @Inject constructor(
    context: Context,
    @LogPath logPath: File,
    logFileFormatter: LogFileFormatterImpl
) : Timber.DebugTree() {

    private val minLogLevel: Int = android.util.Log.DEBUG

    init {
        FL.init(
            FLConfig.Builder(context)
                .minLevel(FLConst.Level.V)
                .logToFile(true)
                .dir(logPath)
                .retentionPolicy(FLConst.RetentionPolicy.TOTAL_SIZE)
                .formatter(logFileFormatter)
                .maxTotalSize(256 * 1024 * 1024) // 256 MB
                .maxFileCount(7 * 24) // 7 Tage
                .build()
        )

        FL.setEnabled(true)
    }

    override fun isLoggable(tag: String?, priority: Int): Boolean {
        return priority >= minLogLevel
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        when (priority) {
            Log.VERBOSE -> FL.v(tag, message)
            Log.DEBUG -> FL.d(tag, message)
            Log.INFO -> FL.i(tag, message)
            Log.WARN -> FL.w(tag, message)
            Log.ERROR -> {
                if (t != null) FL.e(tag, message, t)
                else FL.e(tag, message)
            }
        }
    }
}
