package de.sowok.webviewcontainer.framework.javascript

import android.webkit.JavascriptInterface

class JavaScriptBridgeImpl(private val delegate: JavaScriptBridge) {

    @JavascriptInterface
    fun postMessage(value: String) {
        delegate.postMessage(value)
    }
}
