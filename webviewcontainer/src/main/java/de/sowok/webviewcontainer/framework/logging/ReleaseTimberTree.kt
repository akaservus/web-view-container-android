package de.sowok.webviewcontainer.framework.logging

import android.content.Context
import de.sowok.webviewcontainer.framework.annotation.LogPath
import java.io.File
import javax.inject.Inject

class ReleaseTimberTree @Inject constructor(
    context: Context,
    @LogPath logPath: File,
    logFileFormatter: LogFileFormatterImpl
) : FileLoggingTimberTree(context, logPath, logFileFormatter)
