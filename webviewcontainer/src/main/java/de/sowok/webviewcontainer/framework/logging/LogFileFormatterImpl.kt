package de.sowok.webviewcontainer.framework.logging

import com.bosphere.filelogger.FLConfig
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject
import kotlin.concurrent.getOrSet

class LogFileFormatterImpl @Inject constructor() : FLConfig.DefaultFormatter(), LogFileFormatter {

    private val date = ThreadLocal<Date>()

    private val timeFmt = ThreadLocal<SimpleDateFormat>()

    override fun formatFileName(timeInMillis: Long): String {
        val date = date.getOrSet { Date() }
        date.time = timeInMillis

        return "WebViewContainer-${timeFmt.getOrSet {
            SimpleDateFormat("YYYYMMdd-HH", Locale.ENGLISH)
        }.format(date)}.log"
    }
}
