package de.sowok.webviewcontainer.framework.inject.module

import dagger.Binds
import dagger.Module
import de.sowok.webviewcontainer.framework.logging.LogFileFormatter
import de.sowok.webviewcontainer.framework.logging.LogFileFormatterImpl

@Module
abstract class LoggingModule {

    @Binds
    abstract fun bindLogFileFormatter(logFileFormatter: LogFileFormatterImpl): LogFileFormatter
}