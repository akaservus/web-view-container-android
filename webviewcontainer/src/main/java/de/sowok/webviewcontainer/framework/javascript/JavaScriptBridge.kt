package de.sowok.webviewcontainer.framework.javascript

interface JavaScriptBridge {
    fun postMessage(value: String)
}
