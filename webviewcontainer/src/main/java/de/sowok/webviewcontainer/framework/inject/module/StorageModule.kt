package de.sowok.webviewcontainer.framework.inject.module

import android.content.Context
import dagger.Module
import dagger.Provides
import de.sowok.webviewcontainer.framework.annotation.FilesDirectory
import de.sowok.webviewcontainer.framework.annotation.LogPath
import java.io.File
import javax.inject.Singleton

@Module
object StorageModule {
    @Provides
    @JvmStatic
    @FilesDirectory
    @Singleton
    fun provideFilesDir(context: Context): File {
        return context.filesDir
    }

    @Provides
    @JvmStatic
    @LogPath
    fun provideLogDir(@FilesDirectory filesDir: File): File {
        return File(filesDir, "logs")
    }
}