package de.sowok.webviewcontainer.framework.inject.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class WebViewContainerModule(private val context: Context) {
    @Provides
    @Singleton
    fun provideContext(): Context = context
}