package de.sowok.webviewcontainer.framework.inject

import dagger.Component
import de.sowok.webviewcontainer.framework.inject.module.LoggingModule
import de.sowok.webviewcontainer.framework.inject.module.StorageModule
import de.sowok.webviewcontainer.framework.inject.module.WebViewContainerModule
import de.sowok.webviewcontainer.view.WebViewContainer
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        WebViewContainerModule::class,
        LoggingModule::class,
        StorageModule::class
    ]
)
interface WebViewContainerComponent {
    fun inject(target: WebViewContainer)
}