package de.sowok.webviewcontainer.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.webkit.WebView
import android.webkit.WebView.setWebContentsDebuggingEnabled
import android.widget.LinearLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.webkit.WebViewClientCompat
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import de.sowok.webviewcontainer.R
import de.sowok.webviewcontainer.framework.inject.DependencyInjector
import de.sowok.webviewcontainer.framework.javascript.JavaScriptBridge
import de.sowok.webviewcontainer.framework.javascript.JavaScriptBridgeImpl
import de.sowok.webviewcontainer.framework.logging.TimberConfig
import de.sowok.webviewcontainer.viewmodel.WebViewContainerViewModel
import de.sowok.webviewcontainer.viewmodel.model.JavascriptObject
import kotlinx.android.synthetic.main.web_view_container.view.*
import timber.log.Timber
import java.io.BufferedReader
import javax.inject.Inject

class WebViewContainer @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): LinearLayout(context, attrs, defStyleAttr),
        JavaScriptBridge {

    private val viewModel = WebViewContainerViewModel()

    @Inject
    lateinit var timberConfig: TimberConfig

    init {
        // Initialisiere Dagger für diese View
        DependencyInjector().buildComponent(context).inject(this)
        // Konfiguriere Logger
        timberConfig.configure()
        // Konfiguriere Layout
        inflate(context, R.layout.web_view_container, this)
        // Konfigiriere Attribute
        setupAttribute(attrs, defStyleAttr)
        // Konfiguriere Javascript für diese View
        setupJavascript()

        // Client für diese Webview
        webView.setWebViewClient(object : WebViewClientCompat() {
            // Erst wenn die Seite geladen wurde, können wir damit kommunizieren
            override fun onPageFinished(view: WebView?, url: String?) {
                view?.let {
                    injectScriptFile(it)
                }
            }

            private fun injectScriptFile(view: WebView) {
                try {
                    val inputStream = context.assets.open("webviewcontainer.js")
                    inputStream.bufferedReader().use(BufferedReader::readText)
                } catch (e: Exception) {
                    null
                }?.let {
                    view.evaluateJavascript("""javascript:(function() {$it})()""", null)
                }
            }
        })
    }

    fun loadUrl(urlToLoad: String) {
        webView.loadUrl(urlToLoad)
    }

    fun setupAttribute(attrs: AttributeSet? = null, defStyleAttr: Int = 0) {
        val attribute = context.obtainStyledAttributes(attrs, R.styleable.WebViewContainer, defStyleAttr, 0)
        attribute.getString(R.styleable.WebViewContainer_url)?.let { urlToLoad ->
            loadUrl(urlToLoad)
        }
        attribute.recycle()
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun setupJavascript() {
        webView.settings.apply {
            loadWithOverviewMode = true
            databaseEnabled = true
            domStorageEnabled = true
            javaScriptEnabled = true
            javaScriptCanOpenWindowsAutomatically = true
            setAppCacheEnabled(false)
        }
        webView.addJavascriptInterface(JavaScriptBridgeImpl(this), WEBVIEW_HANDLER)
        setWebContentsDebuggingEnabled(true)

        // Höre auf publisher Livedata aus dem Viewmodel
        viewModel.publiher.observe(context as LifecycleOwner, Observer { data ->
            val mapper = ObjectMapper().registerModule(KotlinModule())
            webView.evaluateJavascript(
                "javascript:WebViewContainer.callback('" + mapper.writeValueAsString(data) + "')",
                null
            )
        })
    }

    override fun postMessage(value: String) {
        Timber.i("console String von Web: %s", value)
        val mapper = ObjectMapper().registerModule(KotlinModule())
        val request = mapper.readValue(value, JavascriptObject::class.java)
        viewModel.consumeRequest(request)
    }

    companion object {
        private const val WEBVIEW_HANDLER = "WebViewContainerHandler"
    }
}