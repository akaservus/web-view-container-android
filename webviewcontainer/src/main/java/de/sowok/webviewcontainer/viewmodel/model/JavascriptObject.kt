package de.sowok.webviewcontainer.viewmodel.model

import java.io.Serializable

data class JavascriptObject(
    val id: String,
    val method : String,
    val message: Any?
): Serializable
