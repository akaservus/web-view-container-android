package de.sowok.webviewcontainer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.sowok.webviewcontainer.viewmodel.model.JavascriptObject

class WebViewContainerViewModel(): ViewModel() {
    private val _publisher: MutableLiveData<JavascriptObject> = MutableLiveData()

    val publiher: LiveData<JavascriptObject>
        get() = _publisher

    fun consumeRequest(request: JavascriptObject) {
        val result = HashMap<String, String?>()
        result["result"] = "Alles Ok"
        result["error"] = null
        val response = JavascriptObject(request.id, request.method, result)
        _publisher.postValue(response)
    }
}