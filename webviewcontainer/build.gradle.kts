plugins {
    id(Plugins.androidLibrary)
    kotlin(Plugins.android)
    kotlin(Plugins.androidExtensions)
    kotlin(Plugins.kapt)
}

android {
    compileSdkVersion(Apps.compileSdk)
    buildToolsVersion(Apps.buildTools)

    defaultConfig {
        minSdkVersion(Apps.minSdk)
        targetSdkVersion(Apps.targetSdk)
        versionCode = Apps.versionCode
        versionName = Apps.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        // sconsumerProguardFiles = "consumer-rules.pro"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libraries.Kotlin.stdlib)
    implementation(Libraries.appcompat)
    implementation(Libraries.AndroidX.ktx)

    implementation(Libraries.daggerAndroid)
    implementation(Libraries.daggerSupport)
    kapt(Libraries.daggerProcessor)
    kapt(Libraries.daggerCompiler)

    implementation(Libraries.AndroidX.webkit)

    implementation(Libraries.timber)
    implementation(Libraries.filelogger)

    implementation(Libraries.coroutinesAndroid)
    implementation(Libraries.jackson)

    testImplementation(Libraries.junit)
    androidTestImplementation(Libraries.AndroidX.testJunit)
    androidTestImplementation(Libraries.AndroidX.espresso)
}
